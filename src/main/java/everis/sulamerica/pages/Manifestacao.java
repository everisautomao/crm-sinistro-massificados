package everis.sulamerica.pages;

import javax.script.ScriptException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

import everis.sulamerica.bean.CrmBean;
import everis.sulamerica.core.BasePage;
import everis.sulamerica.dslcomands.Robo;
import everis.sulamerica.systemactions.Common;
import everis.sulamerica.utils.Utilities;

public class Manifestacao extends BasePage {

	private static String error;
	Common common = new Common();
	private String ambiente = "";

	public String verificaAmbiente() throws ScriptException {
		error = "";
		try {

			String Sandbox_xPath = "//span[text()='Sandbox: ']/../span[2]";
			ambiente = getText.xpath(Sandbox_xPath);
			System.out.println("Ambiente: " + ambiente);
		} catch (Exception e) {
			error = e.getMessage();
		}
		
		return error;
	}

	/**
	 * Buscando apolice com robot
	 * 
	 * @param data
	 * @return
	 */
	public String buscarApolice(CrmBean data) {
		error = "";

		try {
			Robo robo = new Robo();
			String inputPesquisar_xPath = "//input[@title='Pesquise no Salesforce']";
			String bntConta_xPath = "(//a[text()='" + data.getConta() + "'])[1]";
			// String inputApolice_xPath = "//input[@id='campoApolice']";
			// String inputProposta_xPath = "//input[@id='campoProposta']";
			// String inputData_xPath = "//input[@id='campoData']";
			// String inputHora_xPath = "//input[@id='campoHora']";
			// String btnBuscar_xPath = "//input[@value='Buscar']";
			// String linkNumProposta_xPath = "";

			sendKeys.xpath(inputPesquisar_xPath, data.getConta() + Keys.ENTER);
			click.xpath(bntConta_xPath);

			Thread.sleep(7000);
			// Clicando no botao residencial
			robo.click(1150, 300);
			Thread.sleep(3000);

			// Apolice
			if (!data.getapolice().isEmpty()) {
				System.out.println("Apolice: " + data.getapolice());
				robo.click(720, 500);
				Thread.sleep(1000);
				robo.digitar(data.getapolice());
			}
			// proposta
			if (!data.getproposta().isEmpty()) {
				System.out.println("Proposta: " + data.getproposta());
				robo.click(720, 550);
				Thread.sleep(1000);
				robo.digitar(data.getproposta());
			}

			String dataFormatada = data.getdataDoSinistro().replace("/", "");
			System.out.println("Digitando data: " + data.getdataDoSinistro());
			robo.click(35, 620);
			robo.digitar(dataFormatada);

			Thread.sleep(1000);

			String horaFormatada = data.gethoradoSinistro().replace(":", "");
			System.out.println("Digitando hora: " + data.gethoradoSinistro());
			robo.precionar("tab");
			Thread.sleep(300);
			// robo.click(710, 620);
			robo.digitar(horaFormatada);

			System.out.println("Acionando botao Buscar");
			robo.click(690, 680);
			// robo.precionar("tab"); robo.precionar("enter");
			Thread.sleep(10000);

			System.out.println("Acionando link da proposta");

			robo.scrollY(700);
			Thread.sleep(2000);
			robo.click(65, 650);
			/*
			 * for (int i = 0; i < 12; i++) { robo.precionar("tab"); }
			 * robo.precionar("enter");
			 */
			Thread.sleep(12000);
			//EFA.executeAction(Action.WaitForPageToLoad, null);
			System.out.println("Acionando botao nova Manifesetacao");
			robo.click(689, 410);

		} catch (Exception e) {
			error = "Falha ao buscar apolice - " + e.getMessage();
		}
		return error;

	}

	public String criarManifestacao(CrmBean data) throws InterruptedException {
		String error = "";
		try {
			// Dados do Contato
			String inputNomeComunicante_xPath = "//label[text()='Nome do Comunicante']/../../td[2]//input";
			String cmbRelacaoSegurado_xPath = "//label[text()='Relação com Segurado']/../../td[2]//select";
			String inputNomeContato_xPath = "//label[text()='Nome do Contato Principal']/../../td[2]//input";
			String inputTelefoneContato_xPath = "//label[text()='Telefone do Contato Principal']/../../../td[2]//input";
			String inputEmailContato_xPath = "//label[text()='E-mail do Contato Principal']/../../td[2]//input";
			String inputTelefoneAlternativo_xPath = "//label[text()='Telefone Alternativo']/../../../td[4]//input";
			String inputEmailAlternativo_xPath = "//label[text()='E-mail Alternativo 1']/../../td[4]//input";
			String inputEmailAlternativo2_xPath = "//label[text()='E-mail Alternativo 2']/../../td[4]//input";
			String cmbFormaContatoPrincial_xPath = "//label[text()='Forma de Contato (Principal)']/../../td[4]//select";

			// Informações do Sinistro
			String inputCidade_xPath = "//label[text()='Cidade']/../../td[2]//input";
			String cmbUF_xPath = "//label[text()='UF']/../../td[2]//select";
			String inputDescricaoSinistro_xPath = "//label[text()='Descrição do Sinistro']/../../td[2]//textarea";
			String cmbGarantiaContratada_xPath = "//label[text()='Garantia Contratada']/../../td[2]//select";
			String cmbGarantia_xPath = "//label[text()='Garantia']/../../td[2]//select";
			String cmbRelacaoPrestador_xPath = "//label[text()='Relação Prestador']/../../td[2]//select";
			String cmbEfeito_xPath = "//label[text()='Efeito']/../../../td[2]//select";
			String cmbPreAnalise_xPath = "//label[text()='Pré Análise']/../../td[4]//select";
			String cmbAcaoJudicial_xPath = "//label[text()='Ação Judicial']/../../td[4]//select";

			String btnSalvar_xPath = "(//input[@value='Salvar'][@type='submit'])[2]";

			// Thread.sleep(10000);
			// Dados do Contato
			sendKeys.xpath(inputNomeComunicante_xPath, data.getnomeDoComunicante());
			select.xpath(cmbRelacaoSegurado_xPath, data.getrelacaoComSegurado());
			sendKeys.xpath(inputNomeContato_xPath, data.getnomeDoContatoPrincipal());
			sendKeys.xpath(inputTelefoneContato_xPath, data.gettelefoneDoContatoPrincipal());
			sendKeys.xpath(inputEmailContato_xPath, data.getemailDoContatoPrincipal());

			sendKeys.xpath(inputTelefoneAlternativo_xPath, data.gettelefoneAlternativo());
			sendKeys.xpath(inputEmailAlternativo_xPath, data.getemailAlternativoUm());
			sendKeys.xpath(inputEmailAlternativo2_xPath, data.getemailAlternativoDois());
			select.xpath(cmbFormaContatoPrincial_xPath, data.getformaDeContatoPrincipal());
			// Informações do Sinistro

			sendKeys.xpath(inputCidade_xPath, data.getcidade());
			select.xpath(cmbUF_xPath, data.getuf());
			sendKeys.xpath(inputDescricaoSinistro_xPath, data.getdescricaoDoSinistro());
			select.xpath(cmbGarantiaContratada_xPath, data.getgarantiaContratada());
			select.xpath(cmbGarantia_xPath, data.getgarantia());
			select.xpath(cmbRelacaoPrestador_xPath, data.getrelacaoPrestador());
			select.xpath(cmbEfeito_xPath, data.getefeito());

			select.xpath(cmbPreAnalise_xPath, data.getpreAnalise());
			select.xpath(cmbAcaoJudicial_xPath, data.getacaoJudicial());

			click.xpath(btnSalvar_xPath);
			error = verificaCritica();
			// captura numero da manifestacao
			// Depois de usar a data, salva na planilha o proximo dia
			Utilities.getProximoDia(data.getdataDoSinistro());

		} catch (Exception e) {

			error = e.getMessage();
		}
		return error;
	}

	public String verificaCritica() {
		error = "";
		try {
			Thread.sleep(3000);
			System.out.println("Verificando critica do sistema!");
			String btnSalvando = "//input[@value='Salvando']";
			Utilities.changeFrameByObject(btnSalvando, 1);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(btnSalvando)));
			Utilities.changeToParentFrame();

			System.out.println("Acabou de carregar");
			String errorMsgPrincipal_xPath = "//div[@id='errorDiv_ep'][not(contains(@style,'display: none'))]";
			// CAPTURA ERRO DA DIV PRICIPAL
			Utilities.changeFrameByObject(errorMsgPrincipal_xPath, 1);
			if ((boolean) EFA.executeAction(Action.IsElementPresent, new Element("xpath", errorMsgPrincipal_xPath))) {
				String mensagem = getText.xpath(errorMsgPrincipal_xPath);
				System.out.println(mensagem);
				error = mensagem;
			}

		} catch (Exception e) {
			error = e.getMessage();
		}
		return error;
	}

	public String salvarProtoclo(CrmBean data) {
		error = "";
		try {
			String inputNrProtocolo_xPath = "//td[text()='Número do Protocolo']/../td[2]/div";

			String protocolo = getText.xpath(inputNrProtocolo_xPath);
			if (!protocolo.isEmpty()) {
				System.out.println("Protocolo: " + protocolo);
				Utilities.armazenarDadosPlanilha("Protocolo", protocolo);
				data.setProtocolo(protocolo);
			} else {
				error = "Falha ao salvar protocolo";
				return error;
			}

		} catch (Exception e) {
			error = e.getMessage();
		}
		return error;
	}

	public String regularizacao(CrmBean data) {
		error = "";
		try {
			System.out.println("========= Regularização =========");
			String linkManifestacao_xPath = "//td[text()='Manifestação']/../td[2]//a";
			String btnCloseSubGuia = "//li[@class='x-tab-strip-closable caseTab']//a[@class='x-tab-strip-close']";
			
			if(data.getAbrirAviso().equals("Não")){
			// fecha subguia
			click.jsXpath(btnCloseSubGuia);
			}
			
			if (Utilities.isPresent("//h2[text()='Detalhes de Detalhe da Manifestação']", 10)) {
				System.out.println("Detalhes de Detalhe da Manifestação");
			} else {
				System.out.println("Falha ao visualizar detalhes da manifestação");
			}

			error = validaCampos("Pré Análise", data.getvPreAnalise());
			if (!error.isEmpty())
				return error;
			error = validaCampos("Tipo de Regulação", data.getvTipoRegulacao());
			if (!error.isEmpty())
				return error;
			error = validaCampos("Nome do Regulador", data.getvNomeRegulador());
			if (!error.isEmpty())
				return error;
			error = validaCampos("Telefone do Regulador", data.getvTelefoneRegulador());
			if (!error.isEmpty())
				return error;
			error = validaCampos("E-mail do Regulador", data.getvEmailRegulador());
			if (!error.isEmpty())
				return error;

			click.xpath(linkManifestacao_xPath);
			System.out.println("=============================");
			error = common.validandoProprietario(data.getProprietario(),2);
			if (!error.isEmpty())
				return error;
			System.out.println("=============================");
			error = common.validaStatus(data.getStatus());
			if (!error.isEmpty())
				return error;

		} catch (Exception e) {
			error = e.getMessage();
		}
		return error;
	}

	/**
	 * Valida campos na tela de manifestação
	 */
	public String validaCampos(String campo, String valorEsperado) {
		try {
			Thread.sleep(500);
			System.out.println("=============================");
			System.out.println("Valindando " + campo + " [" + valorEsperado + "]");
			String xpath = "//td[text()='" + campo + "']/../td[4]/div";
			
			String valor = getText.xpath(xpath);

			if (valor.contains(valorEsperado)) {
				System.out.println(campo + " validado com sucesso!");
			} else {
				error = campo + ". Esperava [" + valorEsperado + "], Mas recebeu [" + valor + "]";
				return error;
			}
		} catch (Exception e) {
			error = "Valida campos. \n" + e.getMessage();
		}
		return error;
	}

	public String validaStatus(String statusEsperado) {
		try {
			Thread.sleep(500);
			String xpath = "//td[text()='Status']/../td[2]/div";
			String statusRecebido = getText.xpath(xpath);
			if (statusEsperado.isEmpty()) {
				error = "Informe o status esperado";
				return error;
			}
			if (statusRecebido.contains(statusEsperado)) {
				System.out.println("Status [" + statusEsperado + "] validado com sucesso!");
			} else {
				error = "Status. Esperava [" + statusEsperado + "], Mas recebeu [" + statusRecebido + "]";
				return error;
			}
		} catch (Exception e) {
			error = "Valida campos. \n" + e.getMessage();
		}
		return error;
	}

	/*
	 * Este metodo tem como objetivo buscar uma manifestacao atraves do
	 * protocolo
	 */
	public String buscarManifestacao(CrmBean data) {
		try {
			String numeroDoProtocolo = data.getProtocolo();

			if (numeroDoProtocolo.isEmpty()) {
				error = "Informe o numero do protoclo!";
				return error;
			}
			String Pesquisar = "//input[@title='Pesquise no Salesforce']";
			String manifestacao = "//td[text()='" + numeroDoProtocolo + "']/../th/a";
			String linkDetalhesManifestacao_xPath = "//th[text()='Detalhe da Manifestação']/../..//th/a";
			String btnDetalhes_xPath = "//a[@title='Detalhes']";

			clear.xpath(Pesquisar);
			sendKeys.xpath(Pesquisar, numeroDoProtocolo + Keys.ENTER, 1);
			click.xpath(manifestacao, 2);
			click.xpath(btnDetalhes_xPath);

			click.xpath(linkDetalhesManifestacao_xPath);
		} catch (Exception e) {
			error = "Falha ao buscar manifestacao - " + e.getMessage();
		}
		return error;
	}

}
