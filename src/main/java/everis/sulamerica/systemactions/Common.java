package everis.sulamerica.systemactions;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import everis.sulamerica.bean.CrmBean;
import everis.sulamerica.core.BasePage;
import everis.sulamerica.utils.Utilities;

public class Common extends BasePage {

	public static String error = "";
	public static boolean compativel = false; // VERIFICA SE É COMPATIVEL COM
	public static String userAtivo; // NAVEGADOR
	private String numeroDoProtocolo;

	/**
	 * Função responsável por efetuar o Login no Salesforce. Ao logar, ele
	 * verifica se o campo 'Buscar' do Salesforce está presente para retornar
	 * sucesso na função.
	 * 
	 * @param util_baseLocator:
	 *            variável auxiliar para montar string do xpath para execução do
	 *            método 'executeAction'
	 * @param util_URL:
	 *            URL de acesso ao Salesforce
	 * @param util_username:
	 *            usuário de acesso ao Salesforce
	 * @param util_password:
	 *            senha do usuário do Salesforce
	 * @return: retorna sucesso se encontrar o campo 'Buscar' do Salesforce, ou
	 *          mensagem de erro caso não encontre
	 * @throws Exception
	 */
	public String loginAS(CrmBean data) {
		error = "";
		if (!data.getLoginAs().isEmpty()) {
			try {
				// String inputPesquisar_xPath = "//input[@title='Pesquise no
				// Salesforce']";
				String inputPesquisar_xPath = "//input[@id='phSearchInput']";
				String Nome_xPath = "//div[@class='displayName']/a[text()='" + data.getLoginAs() + "']";
				String Seta_xPath = "//a[@id='moderatorMutton']";
				String DetalhesUsuario_xPath = "//span[text()='Detalhes do usuário']/../../a";
				String BtnLogin_xPath = "(//input[@title='Login'])[1]";
				String BtnVoltarAoCrm_xPath = "//a[text()='Voltar para CRM']";

				clear.xpath(inputPesquisar_xPath);
				// DIGITANDO LOGIN NA PESQUISA DO SALESFORCE
				sendKeys.xpath(inputPesquisar_xPath, data.getLoginAs() + Keys.ENTER);

				// AGUARDANDO A PRESENCA E CLICANDO NO NOME ENCONTRADO NA BUSCA
				if (!Utilities.isPresent(Nome_xPath, 10)) {
					error = "Nome do usuario não foi exibido";
					return error;
				}
				click.xpath(Nome_xPath);

				// AGUARDANDO A PRESENCA E CLICANDO NA SETA DETALHES DO USUARIO
				if (!Utilities.isPresent(Seta_xPath, 10)) {
					error = "Seta detalhes do usuario não foi exibida";
					return error;
				}
				click.jsXpath(Seta_xPath, 2);
				Thread.sleep(1000);
				// AGUARDANDO A PRESENCA E CLICANDO NO BOTAO DETALHES DO USUARIO
				if (!Utilities.isPresent(DetalhesUsuario_xPath, 10)) {
					error = "Botão detalhes do usuario não foi exibido";
					return error;
				}
				click.xpath(DetalhesUsuario_xPath);
				Thread.sleep(1000);
				// AGUARDANDO A PRESENCA E CLICANDO NO BOTAO LOGIN
				if (!Utilities.isPresent(BtnLogin_xPath, 10)) {
					error = "Botão login não foi exibido";
					return error;
				}
				click.xpath(BtnLogin_xPath);
				// AGUARDANDO A PRESENCA E CLICANDO NO BOTAO VOLTAR AO CRM
				if (!Utilities.isPresent(BtnVoltarAoCrm_xPath, 10)) {
					error = "Botão voltar ao CRM não foi exibido";
					return error;
				}
				click.jsXpath(BtnVoltarAoCrm_xPath, 3);

				// FECHA TODAS AS ABAS ABERTAS APÓS O LOGIN
				// fecharTodasAbas();
				closeALL();
				System.out.println("Realizado Login As para " + data.getLoginAs());
			} catch (Exception e) {
				error = "Falha no login AS - " + e;
			}
		}
		return error;

	}

	/**
	 * Deixa o salerforce no modo de visualização do CRM
	 * 
	 * @throws Exception
	 */
	public static void chooseViewToCRM() throws Exception {
		String menuArrow = "//div[@class='menuButtonButton']/Div[@class='tsid-buttonArrow mbrButtonArrow']";
		// String menuSelecionado =
		// "//div[@class='menuButtonButton']/span[text()='CRM']";
		String menuParaSelecionar = "//div[@class='mbrMenuItems']/a[text()='CRM']";
		String btnVoltarCRM = "//a[text()='Voltar para CRM']";

		String menuViewCRM = "//tr/td[2]/em/*[@id='ext-gen33']/span/../../../../TD[2]";

		Utilities.changeFrameByObject(menuViewCRM, 0);

		if (!(boolean) EFA.executeAction(Action.IsElementPresent, menuViewCRM)) {
			if ((boolean) EFA.executeAction(Action.IsElementPresent, btnVoltarCRM)) {
				EFA.executeAction(Action.Click, btnVoltarCRM);
			} else {
				EFA.executeAction(Action.Click, menuArrow);
				EFA.executeAction(Action.Click, menuParaSelecionar);
			}

		}

		Utilities.changeToParentFrame();

	}

	/**
	 * Esta função tem como finalidade efetuar pesquisar determinada String no
	 * Salesforce e acessá-la. A função tenta diversas vezes pesquisar um item e
	 * acessá-la dentro de um loop 'for'. Tempo de 1 minuto.
	 * 
	 * @param buscar:
	 *            String de busca
	 * @param controleClick:
	 *            Controle de ação - onde será o click aplicado: "pessoas"
	 *            "manifestacao_numero" "manifestacao_protocolo" "" default
	 * @return: retorna vazio caso encontre a String pesquisada
	 * @throws Exception
	 */
	/*
	 * @Description: Selects a value from the "Lupa" popup
	 * 
	 * @Param: util_baseLocator: base locator for Selenium actions
	 * 
	 * util_value: value to be selected
	 * 
	 * @PreReq: To be logged in
	 * 
	 * The "Lupa" screen should be displayed
	 * 
	 */
	public static Boolean cmpBuscarLupa(String util_baseLocator, String util_value) throws Exception {

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Utilities.changeWindow();

		WebElement frame = EFA.cv_driver.findElement(By.id("searchFrame"));
		EFA.cv_driver.switchTo().frame(frame);

		String Pesquisar = "//INPUT[@placeholder='Pesquisar...']";
		Utilities.changeFrameByObject(Pesquisar, 2);

		EFA.cs_executeAction("sendKeys", util_baseLocator + "xpath", "//INPUT[@placeholder='Pesquisar...']",
				new String[] { util_value });
		EFA.cs_executeAction("click", util_baseLocator + "xpath", "//INPUT[@title='Ir!']", new String[] {});

		Utilities.changeToParentFrame();

		WebElement frame2 = EFA.cv_driver.findElement(By.id("resultsFrame"));
		EFA.cv_driver.switchTo().frame(frame2);

		String resultadoPesquisa = "(//table/tbody//tr/th/a[text()='" + util_value + "'])[1]";

		if ((boolean) EFA.executeAction(Action.IsElementPresent, resultadoPesquisa)) {
			EFA.executeAction(Action.JSExecuteScript, resultadoPesquisa, "arguments[0].click();");
			// EFA.executeAction(Action.Click, resultadoPesquisa);
			Utilities.changeWindow();
			Utilities.changeToParentFrame();
			return true;
		} else if (!EFA.isElementPresent(util_baseLocator + "xpath", "//TABLE[@class='list']//TBODY//TR[2]//TH//A")) {
			EFA.evidenceEnabled = false;
			EFA.cs_executeAction("closeWindow", util_baseLocator + "xpath", "", new String[] {});
			EFA.evidenceEnabled = true;
			Utilities.changeWindow();
			Utilities.changeToParentFrame();
			return false;
		} else {
			EFA.evidenceEnabled = false;
			EFA.cs_executeAction("click", util_baseLocator + "xpath", "//TABLE[@class='list']//TBODY//TR[2]//TH//A",
					new String[] {});
			EFA.evidenceEnabled = true;
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Utilities.changeWindow();
			Utilities.changeToParentFrame();
			return true;
		}
	}

	/*
	 * @Description: Search for the "Manifestação" created by e-mail for subject
	 * 
	 * @Param: util_baseLocator: base locator for Selenium actions
	 * 
	 * util_busca: value to be searched
	 * 
	 * @PreReq: To be logged in
	 * 
	 */

	/**
	 * Esta função tem como finalidade emcerrar todas as abas do salesforce para
	 * continuar com as ações. Isto foi preciso para contornar uma particuladade
	 * do salesforce. Caso haja um mesmo elemento mapeado(com as mesmas
	 * identificações) em outra aba que não seja a que estamos trabalhando
	 * atualmente, o sistema sempre identificará e tentará fazer a ação sob o
	 * primeiro elemento que encontrar, isto é, se tivermos 2 botão salvar, um
	 * na primeira aba e outro na segunda aba, o sistema sempre irá tentar
	 * clicar no botão da primeira aba.
	 * 
	 * @param util_baseLocator:
	 *            variável auxiliar para montar string do xpath para execução do
	 *            método 'executeAction'
	 * @throws Exception
	 */
	public static void cmpClosePrimaryTabs() throws Exception {

		String primaryTab = "(//*[contains(text(),'Fechar')]//ancestor::button)[1]";

		String txtSearch = "//div[@data-aura-class='forceSearchInputDesktop']//input";
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(txtSearch)));

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'selectedListItem')]")));

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class,'selectedListItem')]")));

		Thread.sleep(3000);
		Utilities.changeFrameByObject(primaryTab, 1);

		while (EFA.cv_driver.findElements(By.xpath(primaryTab)).size() > 0) {
			Thread.sleep(1200);
			click.jsXpath(primaryTab);
		}
		Utilities.changeToParentFrame();
	}

	/*
	 * @Description: Get system date and time
	 */
	public static String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	/*
	 * @Description: Create backup of datatable
	 */
	public static void backup() throws IOException, InterruptedException {
		new File("C:\\EfaFramework\\CRM_BACKUP").mkdir();
		String[] args = { "CMD", "/C", "COPY", "/Y", "C:\\EfaFramework\\CRM_Scenarios.xlsx",
				"C:\\EfaFramework\\CRM_BACKUP" };
		Process p = Runtime.getRuntime().exec(args);
		p.waitFor();
	}

	private static String calcDigVerif(String num) {
		Integer primDig, segDig;
		int soma = 0, peso = 10;
		for (int i = 0; i < num.length(); i++)
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;
		if (soma % 11 == 0 | soma % 11 == 1)
			primDig = new Integer(0);
		else
			primDig = new Integer(11 - (soma % 11));
		soma = 0;
		peso = 11;
		for (int i = 0; i < num.length(); i++)
			soma += Integer.parseInt(num.substring(i, i + 1)) * peso--;
		soma += primDig.intValue() * 2;
		if (soma % 11 == 0 | soma % 11 == 1)
			segDig = new Integer(0);
		else
			segDig = new Integer(11 - (soma % 11));
		return primDig.toString() + segDig.toString();
	}

	public static String geraCPF() {
		String iniciais = "";
		Integer numero;
		for (int i = 0; i < 9; i++) {
			numero = new Integer((int) (Math.random() * 10));
			iniciais += numero.toString();
		}
		return iniciais + calcDigVerif(iniciais);
	}

	public static boolean validaCPF(String cpf) {
		if (cpf.length() != 11)
			return false;
		String numDig = cpf.substring(0, 9);
		return calcDigVerif(numDig).equals(cpf.substring(9, 11));
	}

	/**
	 * Função tem como finalidade clicar sob o botão 'Manifestação' no
	 * Salesforce As vezes é preciso deixar este botão como default para o Login
	 * do usuário a ser utilizado. A função não seleciona a opção
	 * 'Manifestação', ela apenas clica sob o botão
	 * 
	 * @return
	 * @throws Exception
	 */
	public String acessarMenuNavegacao(String itemMenu) throws Exception {

		String itemMenu_xpath = "//span[contains(text(),'" + itemMenu + "')][@class='menuLabel']";

		String setaMenuNavegacao = "//span[contains(text(),'Central de Vendas')]/../../../../..//div[@class='slds-context-bar__primary navLeft']//a[@class='slds-context-bar__icon-action']";

		cmpClosePrimaryTabs();
		try {
			click.jsXpath(setaMenuNavegacao, 4);

			click.jsXpath(itemMenu_xpath, 1);
		} catch (Exception e) {
			error = "Falha ao acessar menu de navegacao " + "\n" + e.getMessage();
		}

		return error;
	}

	public static Boolean buscarValorTelaPesquisa(String util_baseLocator, String util_value) throws Exception {

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Utilities.changeWindow();

		WebElement frame = EFA.cv_driver.findElement(By.id("searchFrame"));
		EFA.cv_driver.switchTo().frame(frame);

		String Pesquisar = "//INPUT[@placeholder='Pesquisar...']";

		Utilities.changeFrameByObject(Pesquisar, 2);

		sendKeys.xpath("//INPUT[@placeholder='Pesquisar...']", util_value);

		click.xpath("//INPUT[@title='Ir!']");

		Utilities.changeToParentFrame();

		WebElement frame2 = EFA.cv_driver.findElement(By.id("resultsFrame"));
		EFA.cv_driver.switchTo().frame(frame2);

		if (!EFA.isElementPresent(util_baseLocator + "xpath", "//TABLE[@class='list']//TBODY//TR[2]//TH//A")) {
			EFA.evidenceEnabled = false;
			EFA.cs_executeAction("closeWindow", util_baseLocator + "xpath", "", new String[] {});
			EFA.evidenceEnabled = true;
			Utilities.changeWindow();
			Utilities.changeToParentFrame();
			return false;
		} else {
			EFA.evidenceEnabled = false;
			EFA.cs_executeAction("click", util_baseLocator + "xpath", "//TABLE[@class='list']//TBODY//TR[2]//TH//A",
					new String[] {});
			EFA.evidenceEnabled = true;
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Utilities.changeWindow();
			Utilities.changeToParentFrame();
			return true;
		}
	}

	/**
	 * Esta função tem como finalidade efetuar o Logout da aplicação
	 * 
	 * @throws Exception
	 */
	public void logout() throws Exception {
		System.out.println("Saindo do sistema.");
		String btnLogout = "//div[@id='userNavButton']";
		String menuLogout = "//a[@title='Logout']";
		String btnLogoutNewLayout = "//a[@class='zen-trigger']";

		Utilities.changeFrameByObject(btnLogout, 1);
		if ((boolean) EFA.executeAction(Action.IsElementPresent, btnLogout)) {

			click.xpath(btnLogout);

			// EFA.executeAction(Action.Click, btnLogout);
			Utilities.changeToParentFrame();

			Utilities.changeFrameByObject(menuLogout, 1);
			EFA.executeAction(Action.JSExecuteScript, menuLogout, "arguments[0].click();");
			// EFA.executeAction(Action.Click, menuLogout);
			Thread.sleep(1000);
			if ((boolean) EFA.executeAction(Action.AlertIsPresent, null))
				;
			EFA.executeAction(Action.AlertConfirmationMessage, true);
			Utilities.changeToParentFrame();
			return;
		}
		Utilities.changeToParentFrame();

		Utilities.changeFrameByObject(btnLogoutNewLayout, 1);
		if ((boolean) EFA.executeAction(Action.IsElementPresent, btnLogoutNewLayout)) {
			// EFA.executeAction(Action.Click, btnLogoutNewLayout);
			EFA.executeAction(Action.JSExecuteScript, btnLogoutNewLayout, "arguments[0].click();");
			Utilities.changeToParentFrame();

			Utilities.changeFrameByObject(menuLogout, 1);
			EFA.executeAction(Action.JSExecuteScript, menuLogout, "arguments[0].click();");
			// EFA.executeAction(Action.Click, menuLogout);
			Thread.sleep(1000);
			if ((boolean) EFA.executeAction(Action.AlertIsPresent, null))
				;
			EFA.executeAction(Action.AlertConfirmationMessage, true);
			Utilities.changeToParentFrame();
			return;
		}
		Utilities.changeToParentFrame();
		return;
	}

	public String logoutAs() throws Exception {
		error = "";
		try {
			String iconUser = "//div[@id='userNavButton']";
			String fazerLogout = "//a[@title='Fazer logout']";

			click.xpath(iconUser);
			click.xpath(fazerLogout);
		} catch (Exception e) {
			error = e.getMessage();
		}
		return error;
	}

	public static void acessarModoExibicao(String modoExibicao) throws Exception {

		String selectModoExibicao = "//select[@name='fcf']";
		String btnNovo = "//INPUT[@title='Novo']";

		Utilities.changeFrameByObject(selectModoExibicao, 1);
		EFA.executeAction(Action.SelectByVisibleText, selectModoExibicao, modoExibicao);
		Utilities.changeToParentFrame();

		int timeout = 5;
		for (int i = 0; i <= timeout; i++) {
			Utilities.changeFrameByObject(btnNovo, 0);
			if ((boolean) EFA.executeAction(Action.IsDisplayed, btnNovo)) {
				Thread.sleep(500);
				return;
			} else
				Thread.sleep(500);
			Utilities.changeToParentFrame();
		}
	}

	public static String pesquisarProtocoloModoExibicao(String protocolo) throws Exception {

		String btnPrimeiraPagina = "//img[@title='Primeira página']";
		String btnAvancar = "//a[text()='Avançar']";
		String protocoloEncontrado = "//div[@title='Protocolo']/../../../../../../../following-sibling::div//div//a[text()='"
				+ protocolo + "']";

		Utilities.changeFrameByObject(btnPrimeiraPagina, 0);
		if ((boolean) EFA.executeAction(Action.IsElementPresent, btnPrimeiraPagina)) {
			EFA.executeAction(Action.JSExecuteScript, btnPrimeiraPagina, "arguments[0].click();");
		}
		Utilities.changeToParentFrame();

		boolean avancar = true;

		while (avancar) {
			Utilities.changeFrameByObject(protocoloEncontrado, 1);
			if ((boolean) EFA.executeAction(Action.IsElementPresent, protocoloEncontrado)) {
				EFA.executeAction(Action.JSExecuteScript, protocoloEncontrado, "arguments[0].scrollIntoView(true);");
				EFA.cs_getTestEvidence("Guia encontrada", 1);
				EFA.executeAction(Action.Click, protocoloEncontrado);
				Utilities.changeToParentFrame();
				return "Guia encontrada";
			} else {
				Utilities.changeToParentFrame();
				Utilities.changeFrameByObject(btnAvancar, 0);
				if ((boolean) EFA.executeAction(Action.IsElementPresent, btnAvancar)) {
					EFA.executeAction(Action.JSExecuteScript, btnAvancar, "arguments[0].click();");
					Utilities.changeToParentFrame();
				} else {
					Utilities.changeToParentFrame();
					avancar = false;
				}
			}
		}
		return "Guia não encontrada";
	}

	public static String acessarMenuManifestacoes() throws Exception {

		String btnNovo = "//INPUT[@title='Novo']";
		boolean retorno = false;

		try {
			Actions build = new Actions(EFA.cv_driver);
			build.moveToElement(EFA.cv_driver.findElement(By
					.xpath("(//tbody[@class='x-btn-small x-btn-icon-small-left']//button[contains(@id,'ext-gen')])[1]")))
					.moveByOffset(124, 0).click().build().perform();

			build.click(
					EFA.cv_driver.findElement(By.xpath("//span[@class='x-menu-item-text' and text()='Manifestações']")))
					.click().build().perform();

			Thread.sleep(1000);

			if ((boolean) EFA.executeAction(Action.AlertIsPresent, null))
				;
			EFA.executeAction(Action.AlertConfirmationMessage, true);

		} catch (Exception e) {
			String error = "Erro ao acessar o menu de Manifestações " + e.getMessage();
			System.out.println(error);
			return error;
		}

		for (int i = 0; i <= 10; i++) {
			Utilities.changeFrameByObject(btnNovo, 1);
			if ((boolean) EFA.executeAction(Action.IsDisplayed, btnNovo)) {
				Utilities.changeToParentFrame();
				retorno = true;
				break;
			}
			Utilities.changeToParentFrame();
		}

		if (retorno)
			return "";
		else
			return "Erro ao acessar menu de Manifestações. Botão 'Novo' não foi exibido.";
	}

	/**
	 * Efetua Login inicial
	 * 
	 * @return Retorna o erro se apresentado.
	 */
	public String login(CrmBean data) throws Exception {
		error = "";
		try {
			String vLogin_Xpath = "//input[@id='username']";
			String vSenha_Xpath = "//input[@id='password']";
			String vbtnEntrar_Xpath = "//*[@id='Login']";

			EFA.cv_driver.get(data.getUrl());

			sendKeys.xpath(vLogin_Xpath, data.getUser());
			sendKeys.xpath(vSenha_Xpath, data.getPassword());
			click.xpath(vbtnEntrar_Xpath);

		} catch (Exception e) {
			error = "LOGIN: Nao foi possivel efetuar login - " + e.getMessage();
			return error;
		}
		closeALL();
		return error;

	}

	/**
	 * realiza inicialização do IEdriver e suas configurações assim como
	 * reconfiguração dos drivers wait, e Js.
	 * 
	 * @throws Exception
	 */

	public void inicializacao(String navegador) throws Exception {

		EFA.loadExecutionInfo();
		if (navegador.equals("CHROME")) {
			System.out.println("Iniciando Chrome");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\driver\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--incognito");
			options.addArguments("--start-maximized");
			options.addArguments("--disable-infobars");
			options.addArguments("--disable-notifications");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			EFA.cv_driver = new ChromeDriver(capabilities);
		}
		if (navegador.equals("FIREFOX")) {
			System.out.println("Iniciando Firefox");
			//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			System.setProperty("webdriver.gecko.driver",
			System.getProperty("user.dir") + "\\driver\\geckodriver.exe");
			
			DesiredCapabilities capabilities=DesiredCapabilities.firefox();
		    capabilities.setCapability("marionette", true);
			
			EFA.cv_driver = new FirefoxDriver(capabilities);
		}
		
		
		
		
		BasePage.js = (JavascriptExecutor) EFA.cv_driver;
		BasePage.wait = new WebDriverWait(EFA.cv_driver, 15);

	}

	public void fecharPrimeiraAba() throws Exception {

		String closeTab = "//div[contains(@class,'header')]/div/ul/li[contains(@class,'closable')]/a[contains(@class,'close')]";
		Utilities.changeFrameByObject(closeTab, 1);
		EFA.executeAction(Action.JSExecuteScript, new Element("xpath", closeTab), "arguments[0].click();");
		Utilities.changeToParentFrame();

	}

	public void closeALL() throws Exception {

		Element primaryTab = new Element("xpath",
				"//div[contains(@class,'header')]/div/ul/li[contains(@class,'closable')]");
		@SuppressWarnings("unused")
		Element primaryTabX = new Element("xpath",
				"//div[contains(@class,'header')]/div/ul/li[contains(@class,'closable')]/a[contains(@class,'close')]");
		Element btnNaoSalvar = new Element("xpath", "//button[text()='Não salvar']");

		Element seta = new Element("xpath", "(//div[@class='x-tab-tabmenu-right'])[1]");
		Element fecharTodasAbas = new Element("xpath", "//span[text()='Fechar todas as guias principais']/..");

		Utilities.changeFrameByObject(primaryTab, 1);
		if ((boolean) EFA.executeAction(Action.IsElementPresent, primaryTab)) {
			Utilities.changeToParentFrame();

			Utilities.changeFrameByObject(seta, 1);
			EFA.executeAction(Action.JSExecuteScript, seta, "arguments[0].click();");
			Utilities.changeToParentFrame();

			Utilities.changeFrameByObject(fecharTodasAbas, 1);
			EFA.executeAction(Action.JSExecuteScript, fecharTodasAbas, "arguments[0].click();");
			Utilities.changeToParentFrame();

			Utilities.changeFrameByObject(btnNaoSalvar, 1);
			if ((boolean) EFA.executeAction(Action.IsElementPresent, btnNaoSalvar))
				EFA.executeAction(Action.JSExecuteScript, btnNaoSalvar, "arguments[0].click();");
			Utilities.changeToParentFrame();
		}
		Utilities.changeToParentFrame();

	}

	/**
	 * Esta funcao tem como objetivo alterar os status da manifestacao
	 * 
	 * @param status
	 *            status
	 * @param motivo
	 *            Motivo do status
	 * @param areaPendente
	 *            Area pendente
	 * @return none
	 */
	public String AlteraStatus(String status, String motivo, String areaPendente) {
		error = "";
		try {
			String status_xPath = "//td[text()='Status']/../td[2]";
			String cmbStatus_xPath = "//td[text()='Status']/../td/div/span/select";
			String cmbMotivoStatus = "//td[text()='Motivo do Status']/..//select";
			String cmbAreaPendente = "//select[@title='Área Pendente - Disponível'][not(contains(@style,'display:none'))]";
			String btnOk_xPath = "//h2[text()='Campos dependentes']/../../..//input[@value='OK']";

			click.xpath(status_xPath);
			Thread.sleep(1000);
			doubleClick.xpathMove(status_xPath);
			select.xpath(cmbStatus_xPath, status);
			select.xpath(cmbMotivoStatus, motivo);
			if (!areaPendente.isEmpty())
				select.xpath(cmbAreaPendente, areaPendente);
			click.xpath(btnOk_xPath, 2);

			System.out.println("Status Alterado para " + status);

		} catch (Exception e) {
			error = e.getMessage();
		}
		return error;
	}

	// VERIFICA STATUS DA MANIFESTACAO
	public String validaStatus(String status) {
		try {
			String status_xPath = "//td[text()='Status']/../td/div[text()='" + status + "']";
			Utilities.changeFrameByObject("//td[text()='Status']/../td", 3);

			// Se nao encontrar o status informado, retorna um erro
			if (!(boolean) EFA.executeAction(Action.IsElementPresent, new Element("xpath", status_xPath))) {
				String statusRecebido = "//td[text()='Status']/../td[2]/div";
				statusRecebido = getText.xpath(statusRecebido);
				error = "Status, Esperava [" + status + "].Mas recebeu [" + statusRecebido+"]";
			} else {
				System.out.println("Status validado com sucesso.");
			}

		} catch (Exception e) {
			error = "Falha ao verificar status: " + status + " - " + e;

		}
		Utilities.changeToParentFrame();
		return error;
	}

	public String alteraProprietario(String usuario) {
		try {
			String linkAlterar = "//td[text()='Proprietário da manifestação']/../td//a[text() and text()='[Alterar]']";
			String cmbProprietario = "//label[text()='Proprietário']/../..//select";
			String inputValor = "//input[@title='Nome do proprietário']";
			String btnSalvar = "//input[@value='Salvar' and @type='submit']";

			click.xpath(linkAlterar);
			select.xpath(cmbProprietario, "Usuário");
			click.xpath(inputValor, 1);
			slowSendKeys.xpath(inputValor, usuario);

			click.xpath(btnSalvar);
			Thread.sleep(5000);
		} catch (Exception e) {
			error = e.getMessage();
		}
		return error;
	}

	public void esperaSalvar() throws Exception {

		Thread.sleep(3000);
		String btnSalvando = "//input[@value='Salvando']";
		Utilities.changeFrameByObject(btnSalvando, 1);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(btnSalvando)));
		Utilities.changeToParentFrame();
	}

	/*
	 * Esta funcao tem como finalidade analisar se o sistema exibiu criticas
	 * referentes ao preenchimento dos campos durante a abertura de manifestacao
	 */
	int contagemErros = 0;

	public String verificaErro() throws Exception {
		error = "";
		System.out.println("Verificando se o sistema emitiu alguma critica");
		// String errorMsgPrincipal_xPath = "//div[@class='errorMsg']";
		String errorMsgPrincipal_xPath = "//div[@id='errorDiv_ep' and not(@style='display: none')]";

		contagemErros = 0;
		// CAPTURA ERRO DA DIV PRICIPAL
		Utilities.changeFrameByObject(errorMsgPrincipal_xPath, 1);
		if ((boolean) EFA.executeAction(Action.IsElementPresent, new Element("xpath", errorMsgPrincipal_xPath))) {
			error = getText.xpath(errorMsgPrincipal_xPath);
		}
		// CAPTURA ERRO DE CAMPOS ESPECIFICOS
		error = error + labelError("Nome/Razão Social");
		error = error + labelError("Manifestação pai");
		error = error + labelError("Contrato");
		error = error + labelError("Subtipo");
		error = error + labelError("Assunto");
		error = error + labelError("Cidade");
		error = error + labelError("Forma de Contato");
		Utilities.changeToParentFrame();

		if (error.isEmpty())
			System.out.println("Nenhuma critica apresentada");
		return error;
	}

	public String labelError(String label) {
		String critica = "";
		try {
			String critica_xPath = "//label[text()='" + label + "']/../..//div[@class='errorMsg']";
			Utilities.changeFrameByObject(critica_xPath, 0);
			if ((boolean) EFA.executeAction(Action.IsElementPresent, new Element("xpath", critica_xPath))) {
				contagemErros++;
				critica = "\n (" + contagemErros + ") " + label + " " + getText.xpath(critica_xPath);
			}

		} catch (Exception e) {
			critica = e.getMessage();
		}
		return critica;
	}

	public String esperaSalvarAlteracao() {
		error = "";
		try {
			Thread.sleep(3000);
			String btnSalvando = "//input[@value='Salvando']";
			Utilities.changeFrameByObject(btnSalvando, 1);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(btnSalvando)));
			Utilities.changeToParentFrame();

			System.out.println("Verificando se o sistema emitiu alguma critica...");
			String errorMsgPrincipal_xPath = "//div[@id='errorDiv_ep' and not(@style='display: none')]";
			// CAPTURA ERRO DA DIV PRICIPAL
			Utilities.changeFrameByObject(errorMsgPrincipal_xPath, 1);
			if ((boolean) EFA.executeAction(Action.IsElementPresent, new Element("xpath", errorMsgPrincipal_xPath))) {
				error = getText.xpath(errorMsgPrincipal_xPath);
			}

		} catch (Exception e) {
			error = e.getMessage();
		}
		return error;
	}

	public String validandoProprietario(String proprietario, int time) {
		try {
			
			if(proprietario.isEmpty()){
				error = "Proprietário não foi informado";
				return error;
			}
			
			String proprietarioRecebido = "//td[text()='Proprietário da manifestação']/..//following-sibling::TD[3]//a[text()][not(contains(text(),'[Alterar]'))]";
			Utilities.changeFrameByObject(proprietarioRecebido, 10);
			String proprietarioEsperado_xPath = "//td[text()='Proprietário da manifestação']/..//following-sibling::TD[3]//a[contains(text(),'"
					+ proprietario + "')]";
			for (int i = 1; i <= time; i++) {
				boolean encontrou = EFA.cv_driver.findElements(By.xpath(proprietarioEsperado_xPath)).size() > 0;
				Thread.sleep(1000);
				if (encontrou) {
					System.out.println("proprietario: " + proprietario + " validado com sucesso!");

					break;
					// se nao encontrou o proprieario especificado
				} else {
					System.out.println("Aguardou " + i + " segundo(s)");
					// se o tempo informado chegar ao fim
					if (i >= time) {
						proprietarioRecebido = getText.xpath(proprietarioRecebido);
						if (proprietarioRecebido.isEmpty())
							proprietarioRecebido = "Falha ao capturar proprietario";
						error = "Fila esperada: [" + proprietario + "]. Fila recebida: [" + proprietarioRecebido + "]";
						System.out.println("Aguardou o envido para a fila por " + time + " segundos");
						return error;
					}
				}
			}
			Utilities.changeToParentFrame();
		} catch (Exception e) {
			error = "Falha ao validar proprietario";
		}
		return error;
	}

	public String capturaNumeroDoProtocolo(String ambiente) throws Exception {
		try {
			// XPATH DO PROTOCOLO MUDA DE ACORDO COM AMBIENTE (HML/UAT)
			String numero_xPath = "";
			if (ambiente.contains("UAT")) {
				numero_xPath = "//td[text()='Número do Protocolo' and @class='labelCol']/../../tr[1]/td[2]/div";
			} else {
				numero_xPath = "//td[contains(text(),'Protocolo') and @class='labelCol']/../../tr[1]/td[2]/div";
			}
			numeroDoProtocolo = getText.xpath(numero_xPath);
			System.out.println("Protocolo: " + numeroDoProtocolo + " salvo com sucesso!");
			Utilities.armazenarDadosPlanilha("Protocolo", numeroDoProtocolo);
		} catch (Exception e) {
			error = "Falha ao salvar numero do protoclo";
		}
		return error;
	}

	/*
	 * Este metodo tem como objetivo buscar uma manifestacao
	 */
	public String buscarManifestacao() throws Exception {
		try {
			System.out.println("Buscando manifetação pelo protocolo");
			String Pesquisar = "//input[@title='Pesquise no Salesforce']";
			String manifestacao = "//td[text()='" + numeroDoProtocolo + "']/../th/a";

			clear.xpath(Pesquisar);
			sendKeys.xpath(Pesquisar, numeroDoProtocolo + Keys.ENTER, 1);
			click.xpath(manifestacao, 2);

		} catch (Exception e) {
			error = "Falha ao buscar manifestacao - " + e;
		}
		return error;
	}

	public boolean verificaLogin(String login) {
		boolean logado = false;
		try {
			String login_xPath = "//span[@class='pageMsg highImportance textOnly']";
			String nome = getText.xpath(login_xPath);

			if (nome.contains(login)) {
				logado = true;
			} else {
				logado = false;
			}
		} catch (Exception e) {

		}
		return logado;
	}

	/**
	 * Esta funcao tem como objetivo preencher a causa real via popup
	 * 
	 * @param CausaReal
	 *            Causa Real
	 * @param EspecificacaoCausaReal
	 *            Especificação da Causa Real
	 * @return none
	 */
	public String preencheCausaReal(String causaReal, String especificacaoCausaReal) {
		error = "";
		try {
			System.out.println("Preenchendo Causa Real");
			String linkCausaReal_xPath = "(//td[text()='Causa Real']/../td[2])[1]";
			String cmbCausaReal_xPath = "//td[text()='Causa Real']/..//select";
			String cmbEspecificacaoCausaReal = "//td[text()='Especificação da Causa Real']/..//select";
			String btnOk_xPath = "//h2[text()='Campos dependentes']/../../..//input[@value='OK']";

			click.xpath(linkCausaReal_xPath);
			Thread.sleep(1000);
			doubleClick.xpathMove(linkCausaReal_xPath);
			select.xpath(cmbCausaReal_xPath, causaReal);
			select.xpath(cmbEspecificacaoCausaReal, especificacaoCausaReal);

			click.xpath(btnOk_xPath);

		} catch (Exception e) {
			error = "Falha ao preencher causa real";
		}
		return error;
	}

}
