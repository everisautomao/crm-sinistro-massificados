package everis.sulamerica.dslcomands;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.everis.EFA;

import everis.sulamerica.core.BasePage;
import everis.sulamerica.utils.Utilities;

public class SendKeys extends BasePage {

	public SendKeys xpath(String locator, String value) throws Exception {

		if (!value.isEmpty()) {

			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
			EFA.cv_driver.findElement(By.xpath(locator)).sendKeys(value);
			//EFA.cs_getTestEvidence("Select", 1);
			Utilities.changeToParentFrame();

		}

		return this;
	}

	public SendKeys xpath(String locator, String value, int time) {
		try {
			for (int i = 0; i <= time; i++) {
				Thread.sleep(1000);
			}

			if (!value.isEmpty()) {
				Utilities.changeFrameByObject(locator, 20);
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
				EFA.cv_driver.findElement(By.xpath(locator)).sendKeys(value);
				EFA.cs_getTestEvidence("Select", 1);
				Utilities.changeToParentFrame();
			}
		} catch (Exception e) {
		}
		return this;
	}

	public SendKeys id(String locator, String value) throws Exception {

		if (!value.isEmpty()) {
			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
			EFA.cv_driver.findElement(By.id(locator)).sendKeys(value);
			EFA.cs_getTestEvidence("Select", 1);
			Utilities.changeToParentFrame();
		}

		return this;
	}

	public SendKeys id(String locator, String value, int time) {
		try {
			for (int i = 0; i <= time; i++) {
				Thread.sleep(1000);
			}

			if (!value.isEmpty()) {
				Utilities.changeFrameByObject(locator, 20);
				wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
				EFA.cv_driver.findElement(By.id(locator)).sendKeys(value);
				EFA.cs_getTestEvidence("Select", 1);
				Utilities.changeToParentFrame();
			}
		} catch (Exception e) {
		}
		return this;
	}
}
