package everis.sulamerica.dslcomands;



import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;

import everis.sulamerica.core.BasePage;
import everis.sulamerica.utils.Utilities;

public final class ScrollToElementView extends BasePage {

	public ScrollToElementView xpath(String locator) throws Exception {

		EFA.executeAction(Action.JSExecuteScript, new Element("xpath", locator), "arguments[0].scrollIntoView(true);");

		return this;
	}

	public ScrollToElementView down() throws Exception {

		js.executeScript("window.scrollBy(0,300)", "");
		return this;
	}

	public ScrollToElementView up() throws Exception {

		js.executeScript("window.scrollBy(0,-100)", "");
		return this;
	}

	public ScrollToElementView center(String locator) throws Exception {

		Utilities.changeFrameByObject(locator, 30);
		EFA.executeAction(Action.JSExecuteScript, new Element("xpath", locator),
				"window.scrollBy(0,arguments[0].getBoundingClientRect().y - 200); ").toString();
		Utilities.changeToParentFrame();

		return this;
	}
}
