package everis.sulamerica.dslcomands;

import org.openqa.selenium.By;

import com.everis.EFA;

import everis.sulamerica.utils.Utilities;

public class SlowSendKeys {

	public SlowSendKeys xpath(String locator, String value) throws Exception{

		if (!value.isEmpty()) {
			Utilities.changeFrameByObject(locator, 20);
			String[] separa = value.split("");
			for (int letra = 0; letra < separa.length; letra++) {
				Thread.sleep(40);
				EFA.cv_driver.findElement(By.xpath(locator)).sendKeys(separa[letra]);
			}

			Utilities.changeToParentFrame();
		}

		return this;
	}

	public SlowSendKeys xpath(String locator, String value, int time) {
		try {
			Utilities.changeFrameByObject(locator, 20);
			for (int i = 0; i <= time; i++) {
				Thread.sleep(1000);
			}

			if (!value.isEmpty()) {
				String[] separa = value.split("");
				for (int letra = 0; letra < separa.length; letra++) {
					Thread.sleep(40);
					EFA.cv_driver.findElement(By.xpath(locator)).sendKeys(separa[letra]);
				}

				Utilities.changeToParentFrame();
			}
		} catch (Exception e) {
		}
		return this;
	}
}
