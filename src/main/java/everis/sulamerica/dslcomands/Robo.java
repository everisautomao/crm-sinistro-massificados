package everis.sulamerica.dslcomands;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class Robo {

	public void click(int x, int y) {
		try {
			Robot robot = new Robot();
			robot.mouseMove(x, y);
			robot.mousePress(InputEvent.BUTTON1_MASK);
			robot.mouseRelease(InputEvent.BUTTON1_MASK);
		} catch (Exception e) {

		}
	}

	public void digitar(String valor) {

		String[] vetor = valor.split("");

		for (int i = 0; i < vetor.length; i++) {
			tecla(vetor[i]);
		}
	}

	private void tecla(String valor) {
		try {
			Robot robot = new Robot();

			switch (valor) {
			case "0":
				robot.keyPress(KeyEvent.VK_0);
				robot.keyRelease(KeyEvent.VK_0);
				robot.delay(100);
				break;
			case "1":
				robot.keyPress(KeyEvent.VK_1);
				robot.keyRelease(KeyEvent.VK_1);
				robot.delay(100);
				break;

			case "2":
				robot.keyPress(KeyEvent.VK_2);
				robot.keyRelease(KeyEvent.VK_2);
				robot.delay(100);
				break;
			case "3":
				robot.keyPress(KeyEvent.VK_3);
				robot.keyRelease(KeyEvent.VK_3);
				robot.delay(100);
				break;
			case "4":
				robot.keyPress(KeyEvent.VK_4);
				robot.keyRelease(KeyEvent.VK_4);
				robot.delay(100);
				break;
			case "5":
				robot.keyPress(KeyEvent.VK_5);
				robot.keyRelease(KeyEvent.VK_5);
				robot.delay(100);
				break;
			case "6":
				robot.keyPress(KeyEvent.VK_6);
				robot.keyRelease(KeyEvent.VK_6);
				robot.delay(100);
				break;
			case "7":
				robot.keyPress(KeyEvent.VK_7);
				robot.keyRelease(KeyEvent.VK_7);
				robot.delay(100);
				break;
			case "8":
				robot.keyPress(KeyEvent.VK_8);
				robot.keyRelease(KeyEvent.VK_8);
				robot.delay(100);
				break;
			case "9":
				robot.keyPress(KeyEvent.VK_9);
				robot.keyRelease(KeyEvent.VK_9);
				robot.delay(100);
				break;
				
			case "tab":
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);
				robot.delay(100);
				break;

			default:
				break;
			}
			Thread.sleep(300);

		} catch (Exception e) {

		}

	}
	public void precionar(String valor) {
		try {
			Robot robot = new Robot();

			switch (valor) {
			
			case "tab":
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);
				robot.delay(100);
				break;
				
			case "space":
				robot.keyPress(KeyEvent.VK_SPACE);
				robot.keyRelease(KeyEvent.VK_SPACE);
				robot.delay(100);
				break;

			case "enter":
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				robot.delay(100);
				break;
			default:
				break;
			}
			Thread.sleep(300);

		} catch (Exception e) {

		}

	}
	public void scrollY(int distance) {
		try {
			Robot robot = new Robot();

			robot.mouseWheel(distance);
		} catch (Exception e) {

		}

	}
}
