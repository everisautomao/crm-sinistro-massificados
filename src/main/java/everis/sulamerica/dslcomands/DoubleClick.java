package everis.sulamerica.dslcomands;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.everis.EFA;

import everis.sulamerica.core.BasePage;
import everis.sulamerica.utils.Utilities;

public class DoubleClick extends BasePage {
	/**
	 * Realiza ação de click
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @return
	 * @throws Exception
	 */
	public DoubleClick xpath(String locator) throws Exception {
			Utilities.changeFrameByObject(locator, 30);

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));

			new Actions(EFA.cv_driver).doubleClick(EFA.cv_driver.findElement(By.xpath(locator)));

			//EFA.cs_getTestEvidence("Click", 1);

			Utilities.changeToParentFrame();

		return this;
	}

	/**
	 * Realiza ação de click
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @return
	 * @throws Exception
	 */
	public DoubleClick xpathMove(String locator) throws Exception {
		Utilities.changeFrameByObject(locator, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		new Actions(EFA.cv_driver).moveToElement(EFA.cv_driver.findElement(By.xpath(locator))).doubleClick().perform();
		//EFA.cs_getTestEvidence("Click", 1);
		Utilities.changeToParentFrame();
		return this;
	}

	/**
	 * Realiza ação de click
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @return
	 * @throws Exception
	 */
	public DoubleClick xpathMove(String locator, int time) throws Exception {
		for (int i = 0; i <= time; i++) {
			Thread.sleep(1000);
		}
		Utilities.changeFrameByObject(locator, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		new Actions(EFA.cv_driver).moveToElement(EFA.cv_driver.findElement(By.xpath(locator))).doubleClick().perform();
		//EFA.cs_getTestEvidence("Click", 1);
		Utilities.changeToParentFrame();
		return this;
	}

	/**
	 * Realiza ação de click
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @return
	 * @throws Exception
	 */
	public DoubleClick id(String locator) throws Exception {
		Utilities.changeFrameByObject(locator, 20);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
		EFA.cv_driver.findElement(By.id(locator)).click();
		EFA.cs_getTestEvidence("Click", 1);
		Utilities.changeToParentFrame();
		return this;
	}

	/**
	 * Aguarda tempo determinado e Realiza ação de click
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @param time
	 *            Tempo que será esperado antes de clicar em segundos
	 * @return
	 */
	public DoubleClick id(String locator, int time) {
		try {
			for (int i = 0; i <= time; i++) {
				Thread.sleep(1000);
			}
			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
			EFA.cv_driver.findElement(By.id(locator)).click();
			EFA.cs_getTestEvidence("Click", 1);
			Utilities.changeToParentFrame();
		} catch (Exception e) {
		}
		return this;
	}

	/**
	 * Realiza ação de click
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @return
	 * @throws Exception
	 */
	public DoubleClick xpath(String locator, String Value) throws Exception {
		if (!Value.isEmpty()) {
			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
			EFA.cv_driver.findElement(By.xpath(locator)).click();
			EFA.cs_getTestEvidence("Click", 1);
			Utilities.changeToParentFrame();
		}
		return this;
	}

	/**
	 * Aguarda tempo determinado e Realiza ação de click
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @param time
	 *            Tempo que será esperado antes de clicar em segundos
	 * @return
	 */
	public DoubleClick xpath(String locator, int time) {
		try {
			for (int i = 0; i <= time; i++) {
				Thread.sleep(1000);
			}
			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
			EFA.cv_driver.findElement(By.xpath(locator)).click();
			EFA.cs_getTestEvidence("Click", 1);
			Utilities.changeToParentFrame();
		} catch (Exception e) {
		}
		return this;
	}

	/**
	 * Realiza click com javaScript
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @return
	 * @throws Exception
	 */
	public DoubleClick jsXpath(String locator) throws Exception {
		Utilities.changeFrameByObject(locator, 20);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		WebElement element = EFA.cv_driver.findElement(By.xpath(locator));
		js.executeScript("arguments[0].click();", element);
		EFA.cs_getTestEvidence("Click", 1);
		Utilities.changeToParentFrame();
		return this;
	}

	/**
	 * Aguarda tempo determinado e Realiza click com javaScript
	 * 
	 * @param locator
	 *            Elemento que será clicado
	 * @param time
	 *            Tempo que será esperado antes de clicar em segundos
	 * @return
	 */
	public DoubleClick jsXpath(String locator, int time) {
		try {
			for (int i = 0; i <= time; i++) {
				Thread.sleep(1000);
			}
			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(locator)));
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
			WebElement element = EFA.cv_driver.findElement(By.xpath(locator));
			js.executeScript("arguments[0].click();", element);
			EFA.cs_getTestEvidence("Click", 1);
			Utilities.changeToParentFrame();
		} catch (Exception e) {
		}
		return this;
	}

}
