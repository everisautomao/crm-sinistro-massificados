package everis.sulamerica.dslcomands;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.everis.EFA;

import everis.sulamerica.core.BasePage;
import everis.sulamerica.utils.Utilities;

public class Clear extends BasePage {

	public Clear xpath(String locator) throws Exception {
		Utilities.changeFrameByObject(locator, 30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
		EFA.cv_driver.findElement(By.xpath(locator)).clear();
		Utilities.changeToParentFrame();
		return this;
	}

}
