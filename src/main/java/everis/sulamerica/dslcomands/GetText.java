package everis.sulamerica.dslcomands;

import org.openqa.selenium.By;

import com.everis.EFA;

import everis.sulamerica.utils.Utilities;

public class GetText {
	public String xpath(String xpath) throws Exception {

		Utilities.changeFrameByObject(xpath, 30);
		String valueReturn = EFA.cv_driver.findElement(By.xpath(xpath)).getText();
		//EFA.cs_getTestEvidence("GetText", 1);
		Utilities.changeToParentFrame();
		return valueReturn;

	}
	public String value(String xpath) throws Exception {

		Utilities.changeFrameByObject(xpath, 30);
		String valueReturn = EFA.cv_driver.findElement(By.xpath(xpath)).getAttribute("value");
		//EFA.cf_getTestEvidenceWithStep("GetText", 1);
		Utilities.changeToParentFrame();
		return valueReturn;

	}
}
