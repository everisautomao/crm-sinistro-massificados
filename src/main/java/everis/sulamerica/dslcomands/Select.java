package everis.sulamerica.dslcomands;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.everis.EFA;
import everis.sulamerica.core.BasePage;
import everis.sulamerica.utils.Utilities;

public class Select extends BasePage {

	public Select xpath(String locator, String value) throws Exception {

		if (!value.isEmpty()) {
			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
			new org.openqa.selenium.support.ui.Select(EFA.cv_driver.findElement(By.xpath(locator)))
					.selectByVisibleText(value);
			Utilities.changeToParentFrame();
		}
		return this;
	}

	public Select xpath(String locator, String value, int time) {
		try {
			for (int i = 0; i <= time; i++) {
				Thread.sleep(1000);
			}
			if (!value.isEmpty()) {
				Utilities.changeFrameByObject(locator, 20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));

				new org.openqa.selenium.support.ui.Select(EFA.cv_driver.findElement(By.xpath(locator)))
						.selectByVisibleText(value);
				Utilities.changeToParentFrame();
			}
		} catch (Exception e) {
		}
		return this;
	}

	public Select xpath(String locator, String value, String command) throws Exception {

		if (command.equalsIgnoreCase("index")) {
			if (!value.isEmpty()) {
				Utilities.changeFrameByObject(locator, 20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
				new org.openqa.selenium.support.ui.Select(EFA.cv_driver.findElement(By.xpath(locator)))
						.selectByVisibleText(value);
				Utilities.changeToParentFrame();

			}
		}

		return this;
	}

	public Select id(String locator, String value) throws Exception {

		if (!value.isEmpty()) {
			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
			new org.openqa.selenium.support.ui.Select(EFA.cv_driver.findElement(By.id(locator)))
					.selectByVisibleText(value);
			Utilities.changeToParentFrame();
		}
		return this;
	}

	public Select id(String locator, String value, int time) {
		try {
			for (int i = 0; i <= time; i++) {
				Thread.sleep(1000);
			}
			if (!value.isEmpty()) {
				Utilities.changeFrameByObject(locator, 20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
				new org.openqa.selenium.support.ui.Select(EFA.cv_driver.findElement(By.id(locator)))
						.selectByVisibleText(value);
				Utilities.changeToParentFrame();
			}
		} catch (Exception e) {
		}
		return this;
	}

	public Select id(String locator, String value, String command) throws Exception {

		if (command.equalsIgnoreCase("index")) {
			if (!value.isEmpty()) {
				Utilities.changeFrameByObject(locator, 20);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
				new org.openqa.selenium.support.ui.Select(EFA.cv_driver.findElement(By.id(locator)))
						.selectByVisibleText(value);
				Utilities.changeToParentFrame();

			}
		}

		return this;
	}
	public Select value(String locator, String value) throws Exception {

		if (!value.isEmpty()) {
			Utilities.changeFrameByObject(locator, 20);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
			new org.openqa.selenium.support.ui.Select(EFA.cv_driver.findElement(By.xpath(locator)))
					.selectByValue(value);
			Utilities.changeToParentFrame();
		}
		return this;
	}

}
