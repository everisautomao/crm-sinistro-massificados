package everis.sulamerica.utils;

import javax.swing.JOptionPane;

public class StartExecution {
	public static String resp = "";
	public static String label = "";

	public static String telaDefinicaoExecucao() {

		Object[] message = { "Label de Execução: \n" };

		resp = JOptionPane.showInputDialog(message);

		if (resp == null)
			System.exit(0);

		return resp;
	}
}
