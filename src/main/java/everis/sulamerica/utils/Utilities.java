package everis.sulamerica.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.ExecutionInfo;
import com.everis.GlobalData;
import com.everis.data.DataExcel;
import everis.sulamerica.core.BasePage;
import everis.sulamerica.data.google;
import scenario.crm.CrmExecution;

public class Utilities extends BasePage {

	public static GlobalData mv_database = new GlobalData();
	private static Date mv_sceneTime;


	public static google googleData;

	/**
	 * Abrir o excel para realizar manipulações de dados
	 * 
	 * @return Objeto do excel
	 */
	@SuppressWarnings("unused")
	private static DataExcel openExcel() {
		DataExcel excel = new DataExcel(
				new File("data").getAbsoluteFile() + "/" + ExecutionInfo.getTestSuite() + ".xlsx");
		try {
			excel.open();
			excel.readSheet(ExecutionInfo.getTestName());
			return excel;
		} catch (Exception e) {
			e.printStackTrace();
			EFA.cv_onError = e.getMessage();
			return null;
		}
	}

	/**
	 * Armazenar dados na planilha de massa na linha que está sendo executada
	 * 
	 * @param campo:
	 *            nome do campo na planilha de dados
	 * @param valor:
	 *            valor a ser salvo na planilha de dados
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void armazenarDadosPlanilha(String campo, String valor) throws IOException, InterruptedException {
		googleData = CrmExecution.googleData;
		googleData.setData(campo, valor);
	}

	public static boolean validaPresencaElement(String xpath, int time) throws InterruptedException {

		boolean presence = false;

		for (int i = 0; i < time; i++) {
			Thread.sleep(1000);
			if (isPresent(xpath)) {
				presence = true;
				break;
			}

		}
		return presence;

	}

	public static boolean isPresent(String xpath) {

		boolean validaLoginErro = EFA.cv_driver.findElements(By.xpath(xpath)).size() > 0;

		return validaLoginErro;

	}

	public static boolean isPresent(String xpath, int time) throws Exception {
		boolean presence = false;

		for (int i = 0; i < time; i++) {
			Utilities.changeFrameByObject(xpath, 1);
			if ((boolean) EFA.executeAction(Action.IsElementPresent, new Element("xpath", xpath))) {
				presence = true;
				break;
			}
			Utilities.changeToParentFrame();
		}
		return presence;
	}

	/**
	 * Esta função irá criar uma basta 'backup' no mesmo diretório deste projeto
	 * e fará um backup da planilha de execução a cada novo test case. Isto
	 * evita que perca a execução caso a planilha corrompa durante alguma
	 * execução
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void dataExcelBackup() throws IOException, InterruptedException {
		String excelPath = new File("data").getAbsolutePath() + "\\" + ExecutionInfo.getTestSuite() + ".xlsx";
		String dataPath = new File("data").getAbsolutePath() + "\\backup";

		new File(dataPath).mkdir();

		String[] args = { "CMD", "/C", "COPY", "/Y", excelPath, dataPath };

		Process p = Runtime.getRuntime().exec(args);

		p.waitFor();
	}

	// ===================================================================================
	// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	// cf_getDiffTime() => calculates the Time difference ------
	// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	// ===================================================================================
	public static String cf_getDiffTime(Date cv_date1, Date cv_date2) {
		if (cv_date2.getTime() - cv_date1.getTime() < 0) {
			Calendar c = Calendar.getInstance();
			c.setTime(cv_date2);
			c.add(Calendar.DATE, 1);
			cv_date2 = c.getTime();
		}
		long ms = cv_date2.getTime() - cv_date1.getTime();
		long diffSeconds = ms / 1000 % 60;
		long diffMinutes = ms / (60 * 1000) % 60;
		long diffHours = ms / (60 * 60 * 1000);
		String hh = String.valueOf(diffHours);
		String mm = String.valueOf(diffMinutes);
		String ss = String.valueOf(diffSeconds);
		if (String.valueOf(diffHours).length() == 1)
			hh = "0" + hh;
		if (String.valueOf(diffMinutes).length() == 1)
			mm = "0" + mm;
		if (String.valueOf(diffSeconds).length() == 1)
			ss = "0" + ss;
		return hh + ":" + mm + ":" + ss;
	}

	/**
	 * Inicia a contagem de tempo da execução do caso de teste
	 */
	public static void mf_startSceneTimer() {
		mv_sceneTime = new Date();
		mv_sceneTime.setTime(System.currentTimeMillis());
	}

	/**
	 * @Description Encerra a contagem de tempo da execução do caso de teste
	 * 
	 * @return: Retorna o tempo decorrido da execução
	 */
	public static Date mf_endSceneTimer() {
		Date sceneTimeCurrent = new Date();
		sceneTimeCurrent.setTime(System.currentTimeMillis());
		return sceneTimeCurrent;
	}

	/**
	 * @throws InterruptedException
	 * @throws IOException
	 * @Description Armazena na planilha o tempo de execução do caso de teste
	 */
	public static void ms_writeTimeOutput() throws IOException, InterruptedException {
		armazenarDadosPlanilha("Execution Time", cf_getDiffTime(mv_sceneTime, mf_endSceneTimer()));
	}

	/**
	 * @Description Get system date and time
	 */
	public static String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	/**
	 * @Description Espera pagina ser carregada
	 */
	public static void waitPagToLoad() {

		wait.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		});

	}

	/**
	 * Esta função é responsável por executar a ação de delete
	 * 
	 * @param numero
	 *            de que a ação de delete será executada
	 * @param elemento
	 *            que receberá ação de delete
	 * @throws Exception
	 */
	public static void sendDelete(int num, String element) throws Exception {
		for (int i = 0; i < num; i++) {

			sendKeys.xpath(element, "" + Keys.BACK_SPACE);
		}
	}

	/**
	 * @Description Separa uma String por um separador especifico
	 * @return Retorna string com o valor serparado
	 */

	public static String separaPalavra(String palavra, String separador, int numeroArray) {

		String[] vetPremio = new String[40];

		vetPremio = palavra.split(separador);

		String Teste = vetPremio[numeroArray];

		return Teste;
	}


	public static String countSpace(int val) {

		int length = (int) (Math.log10(val) + 1);

		String space = "   |";

		if (length == 2) {

			space = "  |";
		}
		if (length == 3) {

			space = " |";
		}

		return space;
	}

	/**
	 * Muda a na coluna "RunTest" o valor de "Yes" para "No" sendo assim a
	 * maquina que executar não entrará em concorrência com a execução atual
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */

	public static void bloqueiaExecucaoRegistroNoBanco() throws IOException, InterruptedException {

		armazenarDadosPlanilha("Run Test?", "No");

	}

	/**
	 * Procura um objeto dentro dos frames do sistema
	 * 
	 * @param type
	 * @param locator
	 * @param timeOut
	 * @throws Exception
	 */
	public static void changeFrameByObject(String type, String locator, int timeOut) throws Exception {
		String temp = locator;
		changeFrameByObject(temp, timeOut);
	}

	/**
	 * Procura um objeto dentro dos frames do sistema
	 * 
	 * @param objeto
	 * @param timeOut
	 *            - Tempo esperado antes de iniciar a busca
	 * @throws Exception
	 */
	public static void changeFrameByObject(String objeto, int timeOut) throws Exception {
		boolean retorno = false;
		timeOut = timeOut * 1000;
		int relogio = 0;

		while (!(boolean) EFA.executeAction(Action.IsElementPresent, new Element("xpath", objeto))) {
			int i = 0;
			while (true) {
				try {
					EFA.cv_driver.switchTo().frame(i);
					if ((boolean) EFA.executeAction(Action.IsElementPresent, new Element("xpath", objeto))) {
						EFA.executeAction(Action.JSExecuteScript, new Element("xpath", objeto),
								"arguments[0].scrollIntoView(true);");
						retorno = true;
						break;
					} else {
						EFA.cv_driver.switchTo().parentFrame();
					}
				} catch (Exception e) {
					retorno = false;
					break;
				}
				i++;
			}

			if (retorno) {
				return;
			} else {
				Thread.sleep(100);
				relogio = relogio + 100;
				if (relogio > timeOut)
					return;
			}
		}
	}

	/**
	 * Define como frame da execução o frame pai do que está ativo no momento
	 */
	public static void changeToParentFrame() {
		EFA.cv_driver.switchTo().parentFrame();
	}

	/**
	 * Troca a janela da execução
	 */
	public static void changeWindow() {

		int test = 1;
		Set<String> janelas = EFA.cv_driver.getWindowHandles();
		for (String janela : janelas) {

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (test == 2) {
				EFA.cv_driver.switchTo().window(janela);
			} else {
				test++;
			}

		}
	}
	public static void changeParentWindow() {

		int test = 1;
		Set<String> janelas = EFA.cv_driver.getWindowHandles();
		for (String janela : janelas) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (test == 1) {
				EFA.cv_driver.switchTo().window(janela);
				break;
			} else {
				test++;
			}

		}
	}

	/**
	 * Finaliza somente a aba atual
	 */
	public static void closeCurrentTab() {
		String originalHandle = EFA.cv_driver.getWindowHandle();
		String nextHandle = "";

		// Do something to open new tabs
		for (String handle : EFA.cv_driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				nextHandle = handle;
			}
		}
		if (EFA.cv_driver.getWindowHandles().size() > 1) {
			EFA.cv_driver.switchTo().window(originalHandle);
			EFA.cv_driver.close();
			EFA.cv_driver.switchTo().window(nextHandle);
		}
	}

	/**
	 * Simula a ação de pressionar o botão Enter do teclado através da classe
	 * Robot do Java
	 */
	public static void robotEnter() {
		try {
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Calcula a quantidade de dia uteis entre duas datas
	 * 
	 * @param dataInicio
	 * @param dataFim
	 * @return Inteiro com a quantidade de dias uteis
	 * @throws ParseException
	 */
	public static int nuDiasExecutadosUteis(Date dataInicio, Date dataFim, String[] feriados) throws ParseException {

		// Numero de dias que se passaram, sem contar com a data inicio e data
		// fim
		// Observação: Contando que a data inicio e data fim sejam dias uteis

		long nuDiasExecutados = (long) (dataFim.getTime() - dataInicio.getTime()) / 86400000;
		int totalDiasExecutadosUteis = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(dataInicio);
		for (int i = 1; i <= nuDiasExecutados; i++) {

			// Verifica se não é dia util
			if (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
					&& cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				totalDiasExecutadosUteis += 1;
			}

			/* Feriado */
			for (int n = 0; n < feriados.length; n++) {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				String StringDate = format.format(cal.getTime());

				if (StringDate.equals(feriados[n])) {
					totalDiasExecutadosUteis -= 1;
				}
			}

			// Acrescenta mais um dia na data para poder verificar se é dia util
			cal.add(Calendar.DATE, 1);

		}

		return totalDiasExecutadosUteis;
	}

	/**
	 * Completa uma string com zeros a esquerda
	 * 
	 * @param valor
	 * @param totalCaracteres
	 *            - Quantidade de caracteres que a string deve conter
	 * @return Valor com zeros a esquerda
	 */
	public static String completarZeroEsquerda(String valor, int totalCaracteres) {
		for (int i = valor.length(); i < totalCaracteres; i++) {
			valor = "0" + valor;
		}

		return valor;
	}

	/**
	 * Esta função irá esperar um alerta aparecer pelo tempo que for determinado
	 * e retornará True ou False.
	 * 
	 * @throws throws
	 *             Exception
	 * @return True ou False
	 */
	public static boolean waitToAlert(int time) throws Exception {

		for (int i = 0; i <= time; i++) {

			if ((boolean) EFA.executeAction(Action.AlertIsPresent, null)) {
				return true;

			} else {
				Thread.sleep(1000);
			}
		}
		return false;
	}
	
	public static void changeFrameByObject(Element objeto, int timeOut) throws Exception {
		boolean retorno = false;
		timeOut = timeOut * 1000;
		int relogio = 0;

		while (!(boolean) EFA.executeAction(Action.IsElementPresent, objeto)) {
			int i = 0;
			while (true) {
				try {
					EFA.cv_driver.switchTo().frame(i);
					if ((boolean) EFA.executeAction(Action.IsElementPresent, objeto)) {
						EFA.executeAction(Action.JSExecuteScript, objeto, "arguments[0].scrollIntoView(true);");
						retorno = true;
						break;
					} else {
						EFA.cv_driver.switchTo().parentFrame();
					}
				} catch (Exception e) {
					retorno = false;
					break;
				}
				i++;
			}

			if (retorno) {
				return;
			} else {
				Thread.sleep(100);
				relogio = relogio + 100;
				if (relogio > timeOut)
					return;
			}
		}
	}
	/**
	 * @Retorna data atual mais 1
	 */
	@SuppressWarnings("deprecation")
	public static String getProximoDia(String dia) {
		try{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		//Date date = new Date();
		Date data = dateFormat.parse(dia);
		data.setDate(data.getDate() + 1);
		
		dia = dateFormat.format(data);
		Utilities.armazenarDadosPlanilha("Data do Sinistro", dia);
		}catch(Exception e){
			
		}
		return dia;
	}

}
