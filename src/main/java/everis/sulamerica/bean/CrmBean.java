package everis.sulamerica.bean;

import java.io.Serializable;

import everis.sulamerica.data.google;
import scenario.crm.CrmExecution;

public class CrmBean implements Serializable {
	private static final long serialVersionUID = 3512631978032695880L;
	public static google gdb;

	private String TestCase;
	private String vUrl;
	private String vUser;
	private String vPassword;
	private String vLoginAs;
	private String abrirAviso;
	private String protocolo;
	
	private String conta;
	private String apolice;
	private String proposta;
	private String dataDoSinistro;
	private String horadoSinistro;
	private String numeroDaProposta;

	private String nomeDoComunicante;
	private String relacaoComSegurado;
	private String nomeDoContatoPrincipal;
	private String telefoneDoContatoPrincipal;
	private String emailDoContatoPrincipal;

	private String telefoneAlternativo;
	private String emailAlternativoUm;
	private String emailAlternativoDois;
	private String formaDeContatoPrincipal;

	private String cidade;
	private String uf;
	private String descricaoDoSinistro;
	private String garantiaContratada;
	private String garantia;
	private String relacaoPrestador;
	private String efeito;
	private String preAnalise;
	private String acaoJudicial;
	
	//validacoes
	private String vPreAnalise;
	private String vTipoRegulacao;
	private String vNomeRegulador;
	private String vTelefoneRegulador;
	private String vEmailRegulador;
	
	private String proprietario;
	private String status;

	public CrmBean() {

		gdb = CrmExecution.googleData;

		this.TestCase = gdb.getData("TestCase");
		this.vUrl = gdb.getData("vUrl");
		this.vUser = gdb.getData("vUser");
		this.vPassword = gdb.getData("vPassword");
		this.vLoginAs = gdb.getData("Login As");
		
		
		this.protocolo = gdb.getData("Protocolo");
		this.abrirAviso = gdb.getData("Abrir aviso?");
		
		this.conta = gdb.getData("Conta");
		this.apolice = gdb.getData("Apólice");
		this.proposta = gdb.getData("Proposta");
		this.dataDoSinistro = gdb.getData("Data do Sinistro");
		this.horadoSinistro = gdb.getData("Hora do Sinistro");
		this.numeroDaProposta = gdb.getData("Numero da Proposta");

		this.nomeDoComunicante = gdb.getData("Nome do Comunicante");
		this.relacaoComSegurado = gdb.getData("Relação com Segurado");
		this.nomeDoContatoPrincipal = gdb.getData("Nome do Contato Principal");
		this.telefoneDoContatoPrincipal = gdb.getData("Telefone do Contato Principal");
		this.emailDoContatoPrincipal = gdb.getData("E-mail do Contato Principal");

		this.telefoneAlternativo = gdb.getData("Telefone Alternativo");
		this.emailAlternativoUm = gdb.getData("E-mail Alternativo 1");
		this.emailAlternativoDois = gdb.getData("E-mail Alternativo 2        ");
		this.formaDeContatoPrincipal = gdb.getData("Forma de Contato Principal");

		this.cidade = gdb.getData("Cidade");
		this.uf = gdb.getData("UF");
		this.descricaoDoSinistro = gdb.getData("Descrição do Sinistro");
		this.garantiaContratada = gdb.getData("Garantia Contratada");
		this.garantia = gdb.getData("Garantia");
		this.relacaoPrestador = gdb.getData("Relação Prestador");
		this.efeito = gdb.getData("Efeito");
		this.preAnalise = gdb.getData("Pré Análise");
		this.acaoJudicial = gdb.getData("Ação Judicial");
		
		this.vPreAnalise = gdb.getData("vPré Análise");
		this.vTipoRegulacao = gdb.getData("vTipo de Regulação");
		this.vNomeRegulador = gdb.getData("vNome do Regulador");
		this.vTelefoneRegulador = gdb.getData("vTelefone do Regulador");
		this.vEmailRegulador = gdb.getData("vE-mail do Regulador");
		
		this.proprietario = gdb.getData("Proprietário");
		this.status = gdb.getData("Status");

	}

	public String getTestCase() {
		return TestCase;
	}

	public String getUrl() {
		return vUrl;
	}

	public String getUser() {
		return vUser;
	}

	public String getPassword() {
		return vPassword;
	}

	public String getLoginAs() {
		return vLoginAs;
	}
	
	public String getProtocolo() {
		return protocolo;
	}
	public void setProtocolo(String protocolo){
		this.protocolo = protocolo;
	}
	
	public String getAbrirAviso() {
		return abrirAviso;
	}
	public String getConta() {
		return conta;
	}

	public String getapolice() {
		return apolice;
	}

	public String getproposta() {
		return proposta;
	}

	public String getdataDoSinistro() {
		return dataDoSinistro;
	}

	public String gethoradoSinistro() {
		return horadoSinistro;
	}

	public String getnumeroDaProposta() {
		return numeroDaProposta;
	}

	public String getnomeDoComunicante() {
		return nomeDoComunicante;
	}

	public String getrelacaoComSegurado() {
		return relacaoComSegurado;
	}

	public String getnomeDoContatoPrincipal() {
		return nomeDoContatoPrincipal;
	}

	public String gettelefoneDoContatoPrincipal() {
		return telefoneDoContatoPrincipal;
	}

	public String getemailDoContatoPrincipal() {
		return emailDoContatoPrincipal;
	}

	public String gettelefoneAlternativo() {
		return telefoneAlternativo;
	}

	public String getemailAlternativoUm() {
		return emailAlternativoUm;
	}

	public String getemailAlternativoDois() {
		return emailAlternativoDois;
	}

	public String getformaDeContatoPrincipal() {
		return formaDeContatoPrincipal;
	}

	public String getcidade() {
		return cidade;
	}

	public String getuf() {
		return uf;
	}

	public String getdescricaoDoSinistro() {
		return descricaoDoSinistro;
	}

	public String getgarantiaContratada() {
		return garantiaContratada;
	}

	public String getgarantia() {
		return garantia;
	}

	public String getrelacaoPrestador() {
		return relacaoPrestador;
	}

	public String getefeito() {
		return efeito;
	}

	public String getpreAnalise() {
		return preAnalise;
	}

	public String getacaoJudicial() {
		return acaoJudicial;
	}
	public String getvPreAnalise(){return vPreAnalise;}
	public String getvTipoRegulacao(){return vTipoRegulacao;}
	public String getvNomeRegulador(){return vNomeRegulador;}
	public String getvTelefoneRegulador(){return vTelefoneRegulador;}
	public String getvEmailRegulador(){return vEmailRegulador;}
	
	public String getProprietario(){return proprietario;}
	public String getStatus(){return status;}

}
