package everis.sulamerica.data;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import scenario.crm.CrmExecution;

public class google {

	public WebDriver driver;
	public WebDriverWait wait;
	String appURL = "appURL";
	public static String spreadsheetId = "1nVeo9SwA6GPEOz9jBePtKXnhQOOJXsfbfearroK8_ao";
	public static String sheetName = "Execution";
	private static int numRows = 1;
	private int caseRow;
	private static int atualizaValores = 1;
	private static String range = sheetName + "!A:ZZZ";
	private static GoogleSheetAPI sheetAPI = new GoogleSheetAPI();
	private static List<List<Object>> values;

	public int numRow() {

		caseRow = CrmExecution.caseRow;
		int i = 0;
		try {
			while (values.get(0).get(i) != null) {

				i++;
			}
		} catch (IndexOutOfBoundsException e) {
		}
		return i;
	}

	/**
	 * Função Responsávél por inserir dados no Google Sheets
	 * 
	 * 
	 * @param field
	 * @param value
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void setData(String field, String value) throws IOException, InterruptedException {

		SheetsQuickstart.setValue("Test", toAlphabetic(dfGetNumColum(0, field)), caseRow + 1, value);

	}

	@SuppressWarnings("null")
	public int dfGetNumColum(int dvRow, String columName) {
		try {
			for (int j = 0; j <= numRow();) {
				String nomeColuna = values.get(dvRow).get(j).toString();
				if (columName.equalsIgnoreCase(nomeColuna)) {

					return j + 1;

				} else
					j++;
			}
		} catch (Exception e) {

		}
		return (Integer) null;

	}

	public void copyTestCaseExecution() throws IOException, InterruptedException {

		String rangeCopyRow = sheetName + "!A" + (caseRow + 1) + ":FW" + (caseRow + 1);

		 List<List<Object>> rowList = sheetAPI.getSpreadSheetRecords(spreadsheetId,
		 rangeCopyRow);

		 SheetsQuickstart.setValueGeral(rowList, getNumTestCasesToList() + 1);

	}

	public int getNumTestCasesToList() throws IOException, InterruptedException {

		List<List<Object>> rowList = sheetAPI.getSpreadSheetRecords(spreadsheetId, "TestCases!A:ZZZ");
		int i = 0;
		try {
			while (!rowList.get(i).get(0).getClass().toString().isEmpty()) {

				i++;
			}
		} catch (IndexOutOfBoundsException e) {

		}
		return i;
	}

	/**
	 * Função responsável por obter dados da planilha do Google Sheets
	 * 
	 * @param columName
	 * @return
	 */
	public String getData(String columName) {
		try {

			if (numRows == 1) {
				values = sheetAPI.getSpreadSheetRecords(spreadsheetId, range);
				numRows++;
			}

			if (atualizaValores != caseRow) {
				
				System.out.println("Atualizei");
				values = sheetAPI.getSpreadSheetRecords(spreadsheetId, range);
				atualizaValores = caseRow;
			}

			String label = CrmExecution.label;

			if (label.isEmpty()) {

				while (!(values.get(caseRow).get(2).toString()).equalsIgnoreCase("Yes")) {

					caseRow++;

					atualizaValores = caseRow;
					CrmExecution.caseRow = caseRow;

				}
			} else {

				while (!((values.get(caseRow).get(1).toString()).equalsIgnoreCase(label)

						&& (values.get(caseRow).get(2).toString()).equalsIgnoreCase("Yes"))) {


					caseRow++;

					atualizaValores = caseRow;
					CrmExecution.caseRow = caseRow;

				}
			}

			for (int j = 0; j < numRow();) {

				String nomeColuna = values.get(0).get(j).toString();

				if (columName.equalsIgnoreCase(nomeColuna)) {

					return values.get(caseRow).get(j).toString();

				} else
					j++;
			}

		} catch (Exception e) {

			try {

				if (e.getMessage().contains("Index: 0, Size: 0")) {
					System.out.println("Fim de Registros");
					System.exit(0);
				}

				if (e.toString().contains("connect timed out")) {
					System.out.println("opa deu time out");

					for (int j = 0; j < numRow();) {

						String nomeColuna = values.get(0).get(j).toString();
						if (!nomeColuna.equals("")) {
							if (columName.equalsIgnoreCase(nomeColuna)) {

								return values.get(caseRow).get(j).toString();

							} else
								j++;
						} else {
							System.out.println("fim Registro");
						}
					}
				}
			} catch (Exception f) {
				System.out.println(f.getMessage());
			}

		}
		return "";

	}

	/**
	 * Captura nome da coluna por index
	 * 
	 * @param dvRow
	 * @param dvCol
	 * @return nome da coluna ou se não encontrar valor
	 */
	public static String dfGetData(int dvRow, int dvCol) {

		try {
			return values.get(0).get(dvCol).toString();
		} catch (Exception e) {
			return "NoDataFound";
		}

	}

	/**
	 * Captura numero da coluna de acorno com o nome passado
	 * 
	 * @param dv_name
	 * @return numero da coluna
	 */
	public int dfGetColumnName(String dv_name) {
		int dv_col = 0;
		while (dfGetData(0, dv_col) != "NoDataFound") {
			if (dfGetData(0, dv_col).equals(dv_name))
				return dv_col;
			dv_col++;
		}
		return dv_col;
	}

	/**
	 * Realiza a De/Para de numero para caractere alfabetico para uso no momento de
	 * identificar a cecula que deverá ser escrita
	 * 
	 * @param numColum
	 * @return Letra da coluna
	 * 
	 */
	public String toAlphabetic(int numColum) {
		if (numColum < 0) {
			return "-" + toAlphabetic(-numColum - 1);
		}

		int quot = numColum / 26;
		int rem = numColum % 26;
		char letter = (char) ((int) 'A' + (rem - 1));
		if (quot == 0) {
			return "" + letter;
		} else {
			return toAlphabetic(quot) + letter;
		}
	}

	public int getNumTestCases() {

		int i = 0;
		try {
			while (values.get(i).get(0) != null) {

				i++;
			}
		} catch (IndexOutOfBoundsException e) {

		}
		return i;
	}

	public void setCaseRow(int caseRow) {

		this.caseRow = caseRow;

	}

}