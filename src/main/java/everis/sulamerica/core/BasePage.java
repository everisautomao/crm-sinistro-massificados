package everis.sulamerica.core;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.everis.EFA;
import com.everis.Utils;

import everis.sulamerica.dslcomands.Clear;
import everis.sulamerica.dslcomands.Click;
import everis.sulamerica.dslcomands.DoubleClick;
import everis.sulamerica.dslcomands.GetText;
import everis.sulamerica.dslcomands.ScrollToElementView;
import everis.sulamerica.dslcomands.Select;
import everis.sulamerica.dslcomands.SendKeys;
import everis.sulamerica.dslcomands.SlowSendKeys;
import everis.sulamerica.utils.Utilities;

public class BasePage {

	// Variaveis
	protected static final String pathSave = Utils.getOutputDirectory().replaceAll("output/", "").replace("\\", "\\\\");
	public static final String sim = "Sim";
	public static final String nao = "Não";


	// Funções
	protected static WebDriverWait wait = new WebDriverWait(EFA.cv_driver, 10);
	protected static JavascriptExecutor js = (JavascriptExecutor) EFA.cv_driver;
	protected static Click click = new Click();
	protected static DoubleClick doubleClick = new DoubleClick();
	protected static SendKeys sendKeys = new SendKeys();
	protected static SlowSendKeys slowSendKeys = new SlowSendKeys();
	protected static Clear clear = new Clear();
	protected static Select select = new Select();
	protected static GetText getText = new GetText();
	protected static Utilities utils = new Utilities();
	protected static ScrollToElementView ScrollToElementView = new ScrollToElementView();

}
