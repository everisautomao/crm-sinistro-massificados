package scenario.crm;

import org.junit.runner.JUnitCore;

import org.junit.runner.Result;
public class Starter {
	

	public static void main(String[] args) throws Exception {    

		JUnitCore junit = new JUnitCore();
		@SuppressWarnings("unused")
		Result result = junit.run(scenario.crm.CrmExecution.class);
				
	}
}