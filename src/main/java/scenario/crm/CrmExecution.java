package scenario.crm;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.everis.ExecutionInfo;
import com.everis.EFA;
import everis.sulamerica.bean.CrmBean;
import everis.sulamerica.data.google;
import everis.sulamerica.systemactions.Common;
import everis.sulamerica.utils.ScreenCapture;
import everis.sulamerica.utils.Utilities;
import everis.sulamerica.pages.Manifestacao;

public class CrmExecution {
	String mensaje = "";
	String idCasoTeste = "";
	String util_baseLocator = "org.openqa.selenium.By.";
	/**
	 * Script: Web simple form test
	 * 
	 * @param executionName
	 *            Test name
	 * @param data
	 *            Data for current interaction
	 */
	private static Common common;
	private static Manifestacao manifestacao;
	public static int caseRow;
	private static CrmBean data;
	public static google googleData;
	public static String label = "";
	public static CrmExecution execution;

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {

		// Tela inicial de decisão de qual tipo de seguro será executado

		// Carrega toda inicialização do EFA e fecha Chromedriver Para
		// posteriormente ser aberto com configurações
		// de Capabilities

		EFA.cs_startSelenium("CHROME");
		EFA.cv_driver.quit();

		/**
		 * Realiza Validação se será executado utilizado fonte de dados Google
		 * Sheets ou pelo banco de dados
		 */

		// Declaração da função Common
		common = new Common();
		manifestacao = new Manifestacao();
		execution = new CrmExecution();
		// Declaração da função data para chamada das variaveis do banco de
		// dados
		caseRow = 1;

		//
		googleData = new google();

		// Definição do valor de CaseRow na classe GoogleData
		googleData.setCaseRow(caseRow);
		// Enqanto não houver linha na coluna TestCase em branco será
		// executado os casos de testes.

		while (!googleData.getData("TestCase").isEmpty()) {
			if (googleData.getData("Run Test?").equalsIgnoreCase("Yes")) {
				// Realiza inicializacao do chromeDriver e outras
				// configuracoes
				data = new CrmBean();
				execution.beforeTest();
				execution.script();
				execution.afterRunTests();

			}
			caseRow++;
			googleData.setCaseRow(caseRow);

		}
		/*
		 * caseRow++; googleData.setCaseRow(caseRow);
		 */

		System.out.println("Fim de registros");
		EFA.cv_driver.quit();
	}

	/**
	 * 2 Scenario - Precondition for the test
	 * 
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	@Before

	public void beforeTest() throws Exception {
		common.inicializacao("CHROME");
		/*
		 * INPORTANTE -- Bloqueia o registro para que não possa ser executado
		 * por outra maquina, se não for executado sempre será executado o mesmo
		 * caso de teste
		 */
		//Utilities.bloqueiaExecucaoRegistroNoBanco();

		if (googleData.getData("vGerarEvidenciaVideo").equalsIgnoreCase("sim")) {
			ScreenCapture.startRecording(googleData.getData("TestCase"));
		}
		ExecutionInfo.setExecutionName(data.getTestCase());

		// Grava na planilha, quem esta executando o caso de teste
		Utilities.armazenarDadosPlanilha("Result", System.getProperty("user.name") + " esta executando este teste...");
		System.out.println("------------------------------------------------");
		int row = caseRow + 1;
		System.out.println("Linha " + row + " " + data.getTestCase());

		// Inicia contagem de tempo de execução.
		Utilities.mf_startSceneTimer();

	}

	@Test
	public void script() throws Exception {

		try {

			// LOGANDO NO CRM
			this.mensaje = common.login(data);
			if (!this.mensaje.trim().isEmpty())
				return;

			// DETERMINA O AMBIENTE HML/UAT/DEV
			this.mensaje = manifestacao.verificaAmbiente();
			if (!this.mensaje.trim().isEmpty())
				return;

			this.mensaje = common.loginAS(data);
			if (!this.mensaje.trim().isEmpty())
				return;

			if (data.getAbrirAviso().equals("Sim")) {
				this.mensaje = manifestacao.buscarApolice(data);
				if (!this.mensaje.trim().isEmpty())
					return;

				this.mensaje = manifestacao.criarManifestacao(data);
				if (!this.mensaje.trim().isEmpty())
					return;
				this.mensaje = manifestacao.salvarProtoclo(data);
				if (!this.mensaje.trim().isEmpty())
					return;

				this.mensaje = manifestacao.regularizacao(data);
				if (!this.mensaje.trim().isEmpty())
					return;
			}else{
				
				this.mensaje = manifestacao.buscarManifestacao(data);
				if (!this.mensaje.trim().isEmpty())
					return;
				
				this.mensaje = manifestacao.regularizacao(data);
				if (!this.mensaje.trim().isEmpty())
					return;
			}

		} catch (Exception e) {

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			System.out.println(e.getMessage());

			EFA.stackTrace = sw.toString();
			if (!this.mensaje.trim().isEmpty()) {
				this.mensaje = (this.mensaje + " ERRO SCRIPT: " + EFA.stackTrace);
			} else {
				this.mensaje = EFA.stackTrace;
			}
		} finally {
			Utilities.ms_writeTimeOutput();
		}

		Utilities.ms_writeTimeOutput();

	}

	@After
	public void afterRunTests() throws Exception {
		EFA.cv_driver.quit();
		System.out.println("Encerrando Chrome Driver!!!");
		if (googleData.getData("vGerarEvidenciaVideo").equalsIgnoreCase("sim")) {
			ScreenCapture.stopRecording();
		}

		if (this.mensaje.trim().isEmpty()) {
			this.mensaje = googleData.getData("Expected Result");
			Utilities.armazenarDadosPlanilha("Final Result", "Passed");

			System.out.println("Resultado: Passed");
		} else {

			this.mensaje = (Utilities.separaPalavra(mensaje, "---", 0) + EFA.stackTrace);

			/*
			 * File src2 = ((TakesScreenshot)
			 * EFA.cv_driver).getScreenshotAs(OutputType.FILE);
			 * FileUtils.copyFile(src2, new
			 * File(Utils.getPathFromLastExecution() + "/Erro Apresentado.png"
			 * ));
			 */

			Utilities.armazenarDadosPlanilha("Final Result", "Failed");
			System.out.println("Resultado: Failed");
			System.out.println("Descrição Erro: " + mensaje);
		}
		// common.logout();
		ExecutionInfo.setResult(this.mensaje);

		Utilities.armazenarDadosPlanilha("Result", this.mensaje);

		DateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal1 = Calendar.getInstance();
		String date = dateFormat1.format(cal1.getTime());

		Utilities.armazenarDadosPlanilha("ExecutionDate", date);

		googleData.copyTestCaseExecution();

		// Cria evidencia em formato .DOCX
		// GeraEvidencia.create("crm", "CrmSaudeSpExecution",
		// data.getTestCase());

		EFA.evidenceEnabled = false;

		EFA.evidenceEnabled = true;
	}

}
